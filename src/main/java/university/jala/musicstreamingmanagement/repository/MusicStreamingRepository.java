package university.jala.musicstreamingmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import university.jala.musicstreamingmanagement.models.MusicStreamingModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface MusicStreamingRepository extends JpaRepository<MusicStreamingModel, Integer> {
    Optional<List<MusicStreamingModel>> findAllByTitle(String title);
    Optional<List<MusicStreamingModel>> findAllByArtist(String artist);
    Optional<List<MusicStreamingModel>> findAllByGenre(String genre);
    Optional<List<MusicStreamingModel>> findAllByDuration(float duration);
}