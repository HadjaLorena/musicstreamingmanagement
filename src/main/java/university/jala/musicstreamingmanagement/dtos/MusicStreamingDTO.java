package university.jala.musicstreamingmanagement.dtos;

public record MusicStreamingDTO(String title, String artist, String genre, float duration) {
}