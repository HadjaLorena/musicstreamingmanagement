package university.jala.musicstreamingmanagement.models;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import university.jala.musicstreamingmanagement.dtos.MusicStreamingDTO;

@Entity
@Table(name = "music")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class MusicStreamingModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String title;
    private String artist;
    private String genre;
    private float duration;

    public MusicStreamingModel(MusicStreamingDTO musicStreamingDTO){
        this.title = musicStreamingDTO.title();
        this.artist = musicStreamingDTO.artist();
        this.genre = musicStreamingDTO.genre();
        this.duration = musicStreamingDTO.duration();
    }
}