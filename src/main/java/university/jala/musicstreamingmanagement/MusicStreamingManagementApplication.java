package university.jala.musicstreamingmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicStreamingManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(MusicStreamingManagementApplication.class, args);
    }

}
