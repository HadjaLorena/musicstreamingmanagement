package university.jala.musicstreamingmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.musicstreamingmanagement.MusicStreamingManagementApplication;
import university.jala.musicstreamingmanagement.dtos.MusicStreamingDTO;
import university.jala.musicstreamingmanagement.models.MusicStreamingModel;
import university.jala.musicstreamingmanagement.repository.MusicStreamingRepository;
import university.jala.musicstreamingmanagement.services.MusicStreamingService;

import java.util.List;

@RequestMapping("/")

@RestController
public class APIController {

    @Autowired
    private MusicStreamingService musicStreamingService;

    @Autowired
    public APIController(MusicStreamingRepository musicStreamingRepository){

    }

    //A partir daqui, estamos lidando com as operacoes de CRUD no banco de dados

    //Permite adicionar uma música no banco de dados
    @PostMapping
    public ResponseEntity<MusicStreamingModel> createMusic(@RequestBody MusicStreamingDTO musicStreamingDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(musicStreamingService.createMusic(musicStreamingDTO));
    }

    //Permite exibir todas as músicas presentes no bando de dados
    @GetMapping
    public ResponseEntity<List<MusicStreamingModel>> getMusic(){
        return ResponseEntity.ok(musicStreamingService.getMusic());
    }

    //Permite deletar uma música presente no banco de dados pelo id
    @DeleteMapping("/{id}")
    public ResponseEntity<MusicStreamingModel> deleteMusic(@PathVariable int id){
        musicStreamingService.deleteMusic(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    //Permite atualizar uma música presente no banco de dados por meio do id
    @PutMapping("/{id}")
    public ResponseEntity<MusicStreamingModel> updateMusic(@PathVariable int id, @RequestBody MusicStreamingDTO musicStreamingDTO){
        return ResponseEntity.status(HttpStatus.OK).body(musicStreamingService.updateMusic(id, musicStreamingDTO));
    }

    //Permite consultar uma música cadastrada no banco de dados por meio do id
    @GetMapping("/id/{id}")
    public ResponseEntity<MusicStreamingModel> searchById(@PathVariable int id){
        return ResponseEntity.ok(musicStreamingService.searchById(id));
    }

    //Permite consultar uma música cadastrada no banco de dados por meio do title
    @GetMapping("/title/{title}")
    public ResponseEntity<List<MusicStreamingModel>> searchByTitle(@PathVariable String title){
        return ResponseEntity.ok(musicStreamingService.searchByTitle(title));
    }

    //Permite consultar uma música cadastrada no banco de dados por meio do artist
    @GetMapping("/artist/{artist}")
    public ResponseEntity<List<MusicStreamingModel>> searchByArtist(@PathVariable String artist){
        return ResponseEntity.ok(musicStreamingService.searchByArtist(artist));
    }

    //Permite consultar uma música cadastrada no banco de dados por meio do genre
    @GetMapping("/genre/{genre}")
    public ResponseEntity<List<MusicStreamingModel>> searchByGenre(@PathVariable String genre){
        return ResponseEntity.ok(musicStreamingService.searchByGenre(genre));
    }

    //Permite consultar uma música cadastrada no banco de dados por meio do duration
    @GetMapping("/duration/{duration}")
    public ResponseEntity<List<MusicStreamingModel>> searchByDuration(@PathVariable float duration){
        return ResponseEntity.ok(musicStreamingService.searchByDuration(duration));
    }
}
