package university.jala.musicstreamingmanagement.services;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import university.jala.musicstreamingmanagement.dtos.MusicStreamingDTO;
import university.jala.musicstreamingmanagement.models.MusicStreamingModel;
import university.jala.musicstreamingmanagement.repository.MusicStreamingRepository;

import java.util.List;

@Service
public class MusicStreamingService {

    @Autowired
    private MusicStreamingRepository repository;

    public MusicStreamingModel createMusic(@RequestBody MusicStreamingDTO musicStreamingDTO){
        return new MusicStreamingModel(musicStreamingDTO);
    }

    public List<MusicStreamingModel> getMusic(){
        return repository.findAll();
    }

    public void deleteMusic(@PathVariable int id){
        repository.deleteById(id);
    }

    public MusicStreamingModel updateMusic(int id, MusicStreamingDTO musicUpdated){
        MusicStreamingModel updateMusic = repository.findById(id).orElseThrow(()->new EntityNotFoundException("Música não encontrada, verifique se o ID ("+id+") está correto e tente novamente."));

        if(musicUpdated.title() != null) updateMusic.setTitle(musicUpdated.title());
        if(musicUpdated.artist() != null) updateMusic.setArtist(musicUpdated.artist());
        if(musicUpdated.genre() != null) updateMusic.setGenre(musicUpdated.genre());
        if(musicUpdated.duration() > 0) updateMusic.setDuration(musicUpdated.duration());

        return repository.save(updateMusic);
    }

    public MusicStreamingModel searchById(@PathVariable int id){
        return repository.findById(id).orElseThrow();
    }

    public List<MusicStreamingModel> searchByTitle(@PathVariable String title){
        return repository.findAllByTitle(title).orElseThrow();
    }

    public List<MusicStreamingModel> searchByArtist(@PathVariable String artist){
        return repository.findAllByArtist(artist).orElseThrow();
    }

    public List<MusicStreamingModel> searchByGenre(@PathVariable String genre){
        return repository.findAllByGenre(genre).orElseThrow();
    }

    public List<MusicStreamingModel> searchByDuration(@PathVariable float duration){
        return repository.findAllByDuration(duration).orElseThrow();
    }
}